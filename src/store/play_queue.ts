import { ITrack } from '@/models';

// todo: REFACTOR QUEUE
export default {
  namespaced: true,
  state: {
    trackQueue: [],
    currentTrack: null,
  },
  mutations: {
    SET_TRACK_QUEUE: (state: any, tracks: ITrack[]) => state.trackQueue = tracks,
    CLEAR_QUEUE: (state: any) => state.trackQueue.splice(0, state.trackQueue.length),
    DEQUEUE: (state: any) => state.trackQueue.shift(),
  },
  getters: {
    isQueueEmpty: (state: any) => {
      return state.trackQueue.length === 0;
    },
    peek: (state: any) => {
      return state.trackQueue[0];
    },
  },
};
