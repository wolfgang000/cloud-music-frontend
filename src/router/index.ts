import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Signup from '../views/Signup.vue';
import UploadFiles from '../views/home/UploadFiles.vue';
import Dashboard from '../views/home/Dashboard.vue';
import PlayQueue from '../views/home/PlayQueue.vue';
import AlbumList from '@/views/home/AlbumList.vue';
import AlbumDetail from '@/views/home/AlbumDetail.vue';
import ArtistList from '@/views/home/ArtistList.vue';
import ArtistDetail from '@/views/home/ArtistDetail.vue';
import GenreList from '@/views/home/GenreList.vue';
import GenreDetail from '@/views/home/GenreDetail.vue';
import { accounts } from '@/api';

Vue.use(VueRouter);
export const ROUTE_HOME = 'home';
export const ROUTE_UPLOAD_FILES = 'home.upload_files';
export const ROUTE_DASHBOARD = 'home.dashboard';
export const ROUTE_PLAY_QUEUE = 'home.play_queue';
export const ROUTE_ALBUM_LIST = 'home.album_list';
export const ROUTE_ALBUM_DETAIL = 'home.album_detail';
export const ROUTE_ARTIST_LIST = 'home.artist_list';
export const ROUTE_ARTIST_DETAIL = 'home.artist_detail';
export const ROUTE_GENRE_LIST = 'home.genre_list';
export const ROUTE_GENRE_DETAIL = 'home.genre_detail';
export const ROUTE_LOGIN = 'login';
export const ROUTE_SIGNUP = 'signup';


function requireGuest(to: any, from: any, next: any) {
  next(!accounts.isAuthenticated());
}

function requireUser(to: any, from: any, next: any) {
  // will login and come back
  next(accounts.isAuthenticated() ? true : {
    name: ROUTE_LOGIN,
    query: {
      redirect: to.fullPath,
    },
  });
}

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: { name: ROUTE_DASHBOARD },
    beforeEnter: requireUser,
  },
  {
    path: '/home',
    component: Home,
    beforeEnter: requireUser,
    children: [
      {
        path: 'upload_files',
        name: ROUTE_UPLOAD_FILES,
        component: UploadFiles,
      },
      {
        path: 'dashboard',
        name: ROUTE_DASHBOARD,
        component: Dashboard,
      },
      {
        path: 'play_queue',
        name: ROUTE_PLAY_QUEUE,
        component: PlayQueue,
      },
      {
        path: 'albums',
        name: ROUTE_ALBUM_LIST,
        component: AlbumList,
      },
      {
        path: 'albums/:albumId',
        name: ROUTE_ALBUM_DETAIL,
        component: AlbumDetail,
      },
      {
        path: 'artists',
        name: ROUTE_ARTIST_LIST,
        component: ArtistList,
      },
      {
        path: 'artists/:artistId',
        name: ROUTE_ARTIST_DETAIL,
        component: ArtistDetail,
      },
      {
        path: 'genres',
        name: ROUTE_GENRE_LIST,
        component: GenreList,
      },
      {
        path: 'genres/:genreId',
        name: ROUTE_GENRE_DETAIL,
        component: GenreDetail,
      },
    ],
  },
  {
    path: '/account/login',
    name: ROUTE_LOGIN,
    component: Login,
    beforeEnter: requireGuest,
  },
  {
    path: '/account/signup',
    name: ROUTE_SIGNUP,
    component: Signup,
    beforeEnter: requireGuest,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
