export enum PlayerState {
  Prepare = 0,
  Ready,
  Ended,
  Pause,
  Playing,
}

export interface IArtist {
  id: number;
  name: string;
}

export interface IGenre {
  id: number;
  name: string;
}

export interface ITrack {
  id: number;
  title: string;
  mediaUrl: string;
  number: number;
  artist: IArtist;
  album: IAlbumBase;
}

export interface IAlbumBase {
  id: number;
  title: string;
  mediaUrl: string;
  artist: IArtist;
  artists: IArtist[];
}

export interface IAlbumDetail extends IAlbumBase {
  tracks: ITrack[];
}

export function GenNullArtist(): IArtist {
  return {
    id: -1,
    name: '',
  };
}

export function GenNullGenre(): IGenre {
  return {
    id: -1,
    name: '',
  };
}


export function GenNullAlbumBase(): IAlbumDetail {
  return {
    id: -1,
    title: '',
    mediaUrl: '',
    tracks: [],
    artist: GenNullArtist(),
    artists: [],
  };
}

export enum UploadableFileStatus {
  Ready,
  Uploading,
  Finish,
  Error,
}

export interface IUploadableFile {
  tempUUID: string;
  file: File;
  status: UploadableFileStatus;
}

export interface IErrorResponse {
  code: number;
  message: string;
  errors: any[];
}

export interface IGenreWithAlbumsAndArtistsCounts {
  id: number;
  name: string;
  tracksCount: number;
  AlbumsCount: number;
}

export interface IArtistWithAlbumsAndArtistsCounts {
  id: number;
  name: string;
  tracksCount: number;
  AlbumsCount: number;
}

