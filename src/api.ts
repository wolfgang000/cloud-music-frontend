import axios from 'axios';
import toastr from 'toastr';
import { store } from './store';
import router, { ROUTE_LOGIN } from '@/router';

const CURRENT_USER_KEY = 'current_user';

export const cleanUserStateAndRedirectToLogin = (context: any) => {
  accounts.deleteUser();
  context.commit('accounts/SET_CURRENT_USER', null, { root: true });
  context.commit('player/PAUSE_TRACK', null, { root: true });
  router.push({ name: ROUTE_LOGIN });
};

const errorHandler = (error: any) => {
  if (error.response && (error.response.status === 500 || error.response.status > 500)) {
    toastr.error('There was a problem retrieving the data from the server.');
  } else if (error.response && error.response.status === 401)  {
    toastr.warning('it seems your session has expired');
    cleanUserStateAndRedirectToLogin(store);
  }
  throw error;
};

export const general = {
  getTracks() {
    return axios.get(`/api/tracks/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getTracksByArtistId(artistId: number) {
    return axios.get(`/api/tracks/?artistId=${artistId}`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getTracksByGenreId(GenreId: number) {
    return axios.get(`/api/tracks/?genreId=${GenreId}`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getAlbums() {
    return axios.get(`/api/albums/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getAlbumsLatest() {
    return axios.get(`/api/albums/?orderBy=InsertedAt:desc`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getAlbumsByGenreId(genreId: number) {
    return axios.get(`/api/albums/?genreId=${genreId}`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getAlbumsByArtistId(artistId: number) {
    return axios.get(`/api/albums/?artistId=${artistId}`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getAlbum(albumId: number) {
    return axios.get(`/api/albums/${albumId}/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getArtists() {
    return axios.get(`/api/artists/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getArtist(artistId: number) {
    return axios.get(`/api/artists/${artistId}/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getGenres() {
    return axios.get(`/api/genres/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getGenre(genreId: number) {
    return axios.get(`/api/genres/${genreId}/`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  uploadTrack(bodyFormData: FormData) {
    return axios.post('/api/upload_song', bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
      .catch(errorHandler);
  },
};

export const accounts = {
  getUser() {
    const userStr = localStorage.getItem(CURRENT_USER_KEY);
    return userStr ? JSON.parse(userStr) : null;
  },
  setUser(user: string) {
    localStorage.setItem(CURRENT_USER_KEY, JSON.stringify(user));
  },
  deleteUser() {
    localStorage.removeItem(CURRENT_USER_KEY);
  },
  isAuthenticated() {
    if (this.getUser()) {
      return true;
    }
    return false;
  },
  login(email: string, password: string) {
    return axios.post(`/api/auth/login`, { email, password })
      .then((r) => r.data)
      .catch(errorHandler);
  },
  signup(payload: { email: string, password: string }) {
    return axios
      .post(`/api/auth/signup`, payload)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  logout() {
    return axios.post(`/api/auth/logout`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
  getUserDetail() {
    return axios.get(`/api/auth/user_detail`)
      .then((r) => r.data)
      .catch(errorHandler);
  },
};
