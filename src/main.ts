import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store';

import './fonts.scss';
import './general.scss';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'toastr/build/toastr.css';
import './assets/plyr.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faCompactDisc,
  faFileUpload,
  faUser,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faVolumeOff,
  faVolumeUp,
  faPlay,
  faPause,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(
  faCompactDisc,
  faFileUpload,
  faUser,
  faClock,
  faBars,
  faSearch,
  faUserCircle,
  faEnvelope,
  faLock,
  faVolumeOff,
  faVolumeUp,
  faPlay,
  faPause,
  faStepForward,
  faStepBackward,
  faCircleNotch,
  faHome,
);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
