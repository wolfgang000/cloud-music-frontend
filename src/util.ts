export const Util = {
  /**
   * Format the time from seconds to M:SS.
   * @param  {Number} secs Seconds to format.
   * @return {String}      Formatted time.
   */
  formatTime(secs: number) {
    secs = Math.round(secs);
    const minutes = Math.floor(secs / 60) || 0;
    const seconds = secs - minutes * 60 || 0;
    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
  },
  getDefaultCoverImgUrl() {
    return require('./assets/icons/compact-disc.svg');
  },
  isNullOrWhiteSpace(s: any): boolean {
    return !(Boolean(s) && Boolean(s.trim()));
  },
  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      // tslint:disable-next-line:no-bitwise
      const r = Math.random() * 16 | 0;
      // tslint:disable-next-line:no-bitwise
      const v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  },
};
export class Queue<T> extends Array<T> {

  constructor(items: T[] = []) {
    super(...items);
  }

  public enqueue(val: T) {
    this.push(val);
  }

  public dequeue() {
    return this.shift();
  }

  public peek(): T {
    return this[0];
  }

  public isEmpty(): boolean {
    return this.length === 0;
  }

  public clear(): void {
    this.splice(0, this.length);
  }
}
