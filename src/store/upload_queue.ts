import { ITrack, IUploadableFile, UploadableFileStatus, IErrorResponse } from '@/models';
import { general } from '@/api';
import toastr from 'toastr';

const uploadFile = (context: any, file: IUploadableFile)  => {
  const fileIndex = context.state.uploadQueue.findIndex(
    (t: IUploadableFile) => t.tempUUID === file.tempUUID,
  );
  if (fileIndex >= 0) {
    context.state.uploadQueue[fileIndex].status = UploadableFileStatus.Uploading;
  }

  const bodyFormData = new FormData();
  bodyFormData.append('file', file.file);

  general
    .uploadTrack(bodyFormData)
    .then((response: any) => {
      if (fileIndex >= 0) {
        context.state.uploadQueue[fileIndex].status =
          UploadableFileStatus.Finish;
      }
      const nextFile = context.state.uploadQueue.shift();
      if (nextFile) {
        return uploadFile(context, nextFile);
      }
    })
    .catch((errResWrapper: any) => {
      if (errResWrapper.response && errResWrapper.response.status === 400) {
        const errRes: IErrorResponse = errResWrapper.response.data;
        toastr.warning(errRes.message);
      } else if (errResWrapper.response && errResWrapper.response.status === 413) {
        toastr.warning(`The file is too large, the max size is 100 MB per file :${file.file.name}`);
      }

      if (fileIndex >= 0) {
        context.state.uploadQueue[fileIndex].status =
          UploadableFileStatus.Finish;
      }
      const nextFile = context.state.uploadQueue.shift();
      if (nextFile) {
        return uploadFile(context, nextFile);
      }
    });
};

export default {
  namespaced: true,
  state: {
    uploadQueue: [],
    status: UploadableFileStatus.Ready,
  },
  mutations: {
    SET_UPLOAD_QUEUE: (state: any, files: any) => state.uploadQueue = files,
  },
  actions: {
    UPLOAD_FILES: (context: any) => {
      if (context.state.status in [UploadableFileStatus.Ready, UploadableFileStatus.Finish]) {
        context.state.status = UploadableFileStatus.Uploading;
        const nextFile = context.state.uploadQueue.shift();
        if (nextFile) {
          return uploadFile(context, nextFile);
        }
      }
    },
  },
};
