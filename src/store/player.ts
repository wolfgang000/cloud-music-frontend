import { ITrack, PlayerState } from '@/models';

export default {
  namespaced: true,
  state: {
    currentTrack: null,
    playerState: PlayerState.Prepare,
    isMute: false,
    volume: 70,
  },
  mutations: {
    SET_CURRENT_TRACK: (state: any, track: ITrack) => state.currentTrack = track,
    SET_PLAYER_STATE: (state: any, playerState: PlayerState) => state.playerState = playerState,
    SET_IS_MUTE: (state: any, isMute: boolean) => state.isMute = isMute,
    SET_VOLUME: (state: any, volume: number) => state.volume = volume,
    PLAY_TRACK: (state: any) => null,
    PAUSE_TRACK: (state: any) => null,
    RESUME_PLAY_TRACK: (state: any) => null,
  },
  getters: {
    audioSrc: (state: any) => {
      return state.currentTrack ? state.currentTrack.mediaUrl : '';
    },
    trackTitle: (state: any) => {
      return state.currentTrack ? state.currentTrack.title : '';
    },
    trackArtists: (state: { currentTrack: ITrack }) => {
      if (state.currentTrack) {
        const artists = [state.currentTrack.artist, state.currentTrack.album.artist];
        if (artists[0].name === 'Unknown' && artists[1].name === 'Unknown') {
          return [ state.currentTrack.artist ];
        } else if (artists[0].name === 'Unknown' && artists[1].name !== 'Unknown') {
          return [ artists[1] ];
        } else if (artists[0].name !== 'Unknown' && artists[1].name === 'Unknown') {
          return [ artists[0] ];
        } else if (artists[0].id ===  artists[1].id) {
          return [ artists[0] ];
        } else {
          return artists;
        }
      }
      return [];
    },
    isCurrentTrackNull: (state: any) => {
      return state.currentTrack ? false : true;
    },
  },
  actions: {
    PLAY_NEXT_TRACK(context: any) {
      const nextTrack: ITrack = context.rootGetters['play_queue/peek'];
      if (nextTrack) {
        context.commit('play_queue/DEQUEUE', [], { root: true });
        context.commit('SET_CURRENT_TRACK', nextTrack);
        context.commit('PLAY_TRACK');
      }
    },
  },
};
