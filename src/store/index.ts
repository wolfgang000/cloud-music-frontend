import Vue from 'vue';
import Vuex from 'vuex';

import player from './player';
import play_queue from './play_queue';
import home from './home';
import accounts from './accounts';
import upload_queue from './upload_queue';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    player,
    play_queue,
    home,
    accounts,
    upload_queue,
  },
});
